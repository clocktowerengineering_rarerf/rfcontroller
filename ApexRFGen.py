import time
import serial
from AESerial import AESerial

class ApexRFGen(AESerial):


    
    defaultBaud = 19200
    defaultTimeout = .1
    defaultPortID = '/dev/tty_RFGen'
    defaultAddress = 1
    
    # parameter dictionary

    # sets
    
    param_set_rf_off = 1  
    param_set_rf_on  = 2
    param_set_regulation_select = 3 # values to pass 6 = forward power, 7 = load, 8 = ext)
    param_set_forward_power_limit = 4 # 0 to full scale in watts
    param_set_reflected_power_limit = 5 # 0 to 1100W (for 5500 apex) or 20% of rated power
    param_set_external_feedback_limit = 6
    param_set_rf_setpoint = 8 # output set point based on regulation, watts
    param_set_maximum_external_feedback = 9
    param_set_select_active_target = 11 # values 1 to 4
    param_set_target_life = 12 # target life, byte 0 is target number, 1-4 is target life in kwh, 1kwh = 100
    param_set_control_transfer = 14 # select control, 2 = rs232, 4 = analog
    param_set_out_of_setpoint_timer = 15 # seconds that the output doesn't equal setpoint
    param_set_allowable_deviation = 16 # % out of setpoint that starts the timer, value in %.
    param_set_number_of_recipe_steps = 19
    param_set_recipe_step_setpoint = 22 # byte 0 is step, 1 thru 7; byte 1 and 2 is setpoint, lsb first
    param_set_recipe_step_run_time = 23  # byte 0 is step, 1 thru 7; byte 1 and 2 is runtime in hundredths of seconds or joules, lsb first
    param_set_recipe_type = 28 # 1 is time, 2 is joule
    param_set_setpoint_ramping_parameters = 31 # see page 4-80, default is 0
    param_set_arc_suppression_time = 36 # fairly complex, see page 4-81
    param_set_AEbus_watchdog_timer = 39 # not super useful but maybe it's an issue? it's the serial watchdog
    param_set_host_port_timeout_value = 40 # host port timeout
    param_set_arc_suppression_potentiometer_sensitivity_values =84 # see 4-84, not implemented
    param_set_pulsing_frequency = 93 # RF pulsing in Hz, 150 to 50000 but based on part number of unit, not implemented
    param_set_pulsing_duty_cycle = 96 # RF pulsing ON time in %, 1 to 90, minimum based on part number of unit, not implemented

    # reads and gets
    param_read_supply_type = 128 # not implemented
    param_read_supply_size = 129 # not implemented
    param_read_firmware_version_number = 130 # not implemented
    param_read_AEbus_watchdog_timer_value = 139 
    param_read_host_timeout_value = 140
    param_read_set_point_ramping_parameters = 151
    param_read_regulation_mode = 154
    param_read_control_method = 155
    param_read_active_target = 156
    param_read_target_life = 157
    param_read_process_status = 162
    param_read_setpoint_regulation_mode = 164
    param_read_forward_power = 165
    param_read_reflected_power = 166
    param_read_delivered_power = 167
    param_read_external_feedback = 168
    param_read_forward_power_limit = 169
    param_read_reflected_power_limit = 170
    param_read_external_feedback_limit = 171
    param_read_out_of_setpoint_interval = 184
    param_read_allowable_deviation = 185
    param_read_recipe_step_setpoint = 188
    param_read_recipe_step_status = 189
    param_read_pulsing_frequency = 193
    param_read_pulsing_duty_cycle = 196
    param_read_firmware_revision_level = 198 
    param_read_arc_events = 199
    param_read_unit_on_events = 201
    param_read_output_on_events = 202
    param_read_overtemp_events = 203
    param_read_run_time = 205
    param_read_extended_faults = 210
    param_read_pin = 221
    param_read_error_codes = 223
    param_read_cold_plate_temp = 228 # temp in C
    param_read_serial_number = 231
    
    # defines
    max_rf_power = 5500
    forward_power_regulation = 6
    load_power_regulation = 7
    external_power_regulation = 8
    host_control_mode = 2
    user_control_mode = 4
    

    def RFOff(self):
        
        return self.bytes_to_long(self.Send(self.param_set_rf_off,[],self.defaultAddress))
        
    
    def RFOn(self):
        
        return self.bytes_to_long(self.Send(self.param_set_rf_on,[],self.defaultAddress))
        
    def RegulationSelect(self,value):
        
        value = self.BoundInput(value,self.forward_power_regulation,self.external_power_regulation)
        return self.bytes_to_long(self.Send(self.param_set_regulation_select,self.long_to_bytes(value,1),self.defaultAddress))
    
        
    def ForwardPowerLimit(self,value):
        
        value = self.BoundInput(value,0,self.max_rf_power)
        return self.bytes_to_long(self.Send(self.param_set_forward_power_limit,self.long_to_bytes(value,2),self.defaultAddress))
        
    def ReflectedPowerLimit(self,value):
        
        value = self.BoundInput(value,0,self.max_rf_power*0.2)
        return self.bytes_to_long(self.Send(self.param_set_reflected_power_limit,self.long_to_bytes(value,2),self.defaultAddress))
        
    def ExternalPowerLimit(self,value):
        
        value = self.BoundInput(value,0,self.max_rf_power*0.2)
        return self.bytes_to_long(self.Send(self.param_set_external_feedback_limit,self.long_to_bytes(value,2),self.defaultAddress))
             
        
    def RFSetpoint(self, value):
        
        value = self.BoundInput(value,int((self.max_rf_power*.01)),self.max_rf_power)
        return self.bytes_to_long(self.Send(self.param_set_rf_setpoint,self.long_to_bytes(value,2),self.defaultAddress))
        
    def MaxExtFeedback(self,value):

        value = self.BoundInput(value,0,10000)
        return self.bytes_to_long(self.Send(self.param_set_maximum_external_feedback,self.long_to_bytes(value,3),self.defaultAddress))
    
    def SelectActiveTarget(self,target):
        
        target = self.BoundInput(target,1,4)
        return self.bytes_to_long(self.Send(self.param_set_select_active_target,self.long_to_bytes(target,1),self.defaultAddress))
        
    def SetTargetLife(self,value,target=1):
        
        # Todo: do this
        
        target = self.BoundInput(target,1,4)
        value = self.BoundInput(value,0,10000000)
        return self.bytes_to_long(self.Send(self.param_set_target_life,self.long_to_bytes(target,1)+self.long_to_bytes(value,4),self.defaultAddress))
        
    def ControlMethod(self,value=host_control_mode):
        
        value = self.BoundInput(value,self.host_control_mode,self.user_control_mode)
        return self.bytes_to_long(self.Send(self.param_set_control_transfer,self.long_to_bytes(value,1),self.defaultAddress))
    
    def SetpointTimer(self,value=5):
        
        value = self.BoundInput(value,0,599)
        return self.bytes_to_long(self.Send(self.param_set_out_of_setpoint_timer,self.long_to_bytes(value,1),self.defaultAddress))
    
    def AllowableDeviation(self,value=10):
    
        value = self.BoundInput(value,0,99)
        return self.bytes_to_long(self.Send(self.param_set_allowable_deviation,self.long_to_bytes(value,1),self.defaultAddress))
    
    def NumberOfRecipeSteps(self,value=0):
    
        value = self.BoundInput(value,0,7)
        return self.bytes_to_long(self.Send(self.param_set_number_of_recipe_steps,self.long_to_bytes(value,1),self.defaultAddress))
        
    def RecipeStepSetpoint(self,step=0,value=0):

        step = self.BoundInput(step,0,7)
        value = self.BoundInput(value,0,self.max_rf_power)
        return self.bytes_to_long(self.Send(self.param_set_recipe_step_setpoint,(self.long_to_bytes(step,1)+self.long_to_bytes(value,2)),self.defaultAddress))
        
    def RecipeStepRunTime(self,step=0,value=0):
    
        step = self.BoundInput(step,0,7)
        value = self.BoundInput(value,0,65535)
        return self.bytes_to_long(self.Send(self.param_set_recipe_step_run_time,(self.long_to_bytes(step,1)+self.long_to_bytes(value,2)),self.defaultAddress))
        
    def RecipeType(self,value=1):
            
        value = self.BoundInput(value,1,2)
        return self.bytes_to_long(self.Send(self.param_set_recipe_type,self.long_to_bytes(value,1),self.defaultAddress))
        
    def SetpointRampingParameters(self,enable=0,rampup=max_rf_power,rampdown=max_rf_power):
    
        enable = self.BoundInput(enable,0,2)
        rampup = self.BoundInput(rampup,0,65535)
        rampdown = self.BoundInput(rampdown,0,65535)
        return self.bytes_to_long(self.Send(self.param_set_recipe_step_run_time,(self.long_to_bytes(enable,2)+self.long_to_bytes(rampup,2)+self.long_to_bytes(rampdown,2)),self.defaultAddress))
        
    def SetArcSuppressionTime(self,arcsuppressiontime,initialdelaytime,setpointdelaytime,numberofattempts):
    
        arcsuppressiontime = self.BoundInput(arcsuppressiontime,2,511)
        initialdelaytime   = self.BoundInput(initialdelaytime,2,10000)
        setpointdelaytime  = self.BoundInput(setpointdelaytime,2,245)
        numberofattempts   = self.BoundInput(numberofattempts,0,250)
        
        arcresponse = self.Send(self.param_set_arc_suppression_time,[self.long_to_bytes(0,1),self.long_to_bytes(arcsuppressiontime,2)],self.defaultAddress)
        initialdelaytime = self.Send(self.param_set_arc_suppression_time,[self.long_to_bytes(1,1),self.long_to_bytes(initialdelaytime,2)],self.defaultAddress)
        setpointdelaytime = self.Send(self.param_set_arc_suppression_time,[self.long_to_bytes(2,1),self.long_to_bytes(setpointdelaytime,2)],self.defaultAddress)
        numberofattempts = self.Send(self.param_set_arc_suppression_time,[self.long_to_bytes(3,1),self.long_to_bytes(numberofattempts,2)],self.defaultAddress)
        values= (arcresponse+initialdelaytime+setpointdelaytime+numberofattempts)
        return values
        
	def AEBusWatchdog(self,port,value):
		port = self.BoundInput(port,1,2)
        value = self.BoundInput(value,0,65535)
        return self.bytes_to_long(self.bytes_to_long(self.Send(self.param_set_AEbus_watchdog_timer,(self.long_to_bytes(port,1)+self.long_to_bytes(value,2)),self.defaultAddress)))
        
    def HostTimeout(self,value=2):
        
        value = self.BoundInput(value,2,500)
        return self.bytes_to_long(self.Send(self.param_set_host_port_timeout_value,self.long_to_bytes(value,2),self.defaultAddress))
        
    def ReadAEBusWatchdog(self):
                
        return self.bytes_to_long(self.Send(self.param_read_AEbus_watchdog_timer_value,[],self.defaultAddress))
        
    def ReadHostTimeout(self):
        
        return self.bytes_to_long(self.Send(self.param_read_host_timeout_value,[],self.defaultAddress))
        
    def ReadSetpointRampingParameters(self):
        
        return (self.Send(self.param_read_set_point_ramping_parameters,[],self.defaultAddress))
        
    def ReadRegulationMode(self):
        
        return self.bytes_to_long(self.Send(self.param_read_regulation_mode,[],self.defaultAddress))
        
    def ReadControlMethod(self):
        
        return self.bytes_to_long(self.Send(self.param_read_control_method,[],self.defaultAddress))
        
    def ReadActiveTarget(self):
        
        return self.bytes_to_long(self.Send(self.param_read_active_target,[],self.defaultAddress))
        
    def ReadTargetLife(self):
        
        return self.bytes_to_long(self.Send(self.param_read_target_life,[],self.defaultAddress))
        
    def ReadTargetStatus(self):
        
        return self.Send(self.param_read_active_target,[],self.defaultAddress)
        
    def ReadProcessStatus(self):
        
        return self.Send(self.param_read_process_status,[],self.defaultAddress)
        
    def ReadRegulationSelect(self):
        
        value = self.Send(self.param_read_setpoint_regulation_mode,[],self.defaultAddress)
        return self.bytes_to_long(value[2])
        
    def ReadRFSetpoint(self):
        
        value = self.Send(self.param_read_setpoint_regulation_mode,[],self.defaultAddress)
        return self.bytes_to_long(value[:2])
    
    def ReadForwardPower(self):
        
        return self.bytes_to_long(self.Send(self.param_read_forward_power,[],self.defaultAddress))
        
    def ReadReflectedPower(self):
        
        return self.bytes_to_long(self.Send(self.param_read_reflected_power,[],self.defaultAddress))

    def ReadDeliveredPower(self):

        return self.Send(self.param_read_delivered_power,[],self.defaultAddress)
    
    def ReadExternalFeedback(self):
        
        return self.bytes_to_long(self.Send(self.param_read_external_feedback,[],self.defaultAddress))

    def ReadForwardPowerLimit(self):

        return self.bytes_to_long(self.Send(self.param_read_forward_power_limit,[],self.defaultAddress))

    def ReadReflectedPowerLimit(self):
        
        return self.bytes_to_long(self.Send(self.param_read_reflected_power_limit,[],self.defaultAddress))

        
    def ReadSetpointTimer(self):
        
        return self.bytes_to_long(self.Send(self.param_read_out_of_setpoint_interval,[],self.defaultAddress))

        
    def ReadAllowableDeviation(self):
        
        
        return self.bytes_to_long(self.Send(self.param_read_allowable_deviation,[],self.defaultAddress))
        
        
        
    def ReadRecipeSteps(self):
        
        return self.Send(self.param_read_recipe_step_setpoint,[],self.defaultAddress)
        
    def ReadRecipeStatus(self):
        
        return self.Send(self.param_read_recipe_step_status,[],self.defaultAddress)
        
    def ReadFirmware(self):
        
        return self.Send(self.param_read_firmware_revision_level,[],self.defaultAddress)
        
    def ReadArcEvents(self):
        
        return self.bytes_to_long(self.Send(self.param_read_arc_events,[],self.defaultAddress))
        
    def ReadUnitOnEvents(self):
        
        return self.bytes_to_long(self.Send(self.param_read_unit_on_events,[],self.defaultAddress))
        
    def ReadRFOnEvents(self):
        
        return self.bytes_to_long(self.Send(self.param_read_output_on_events,[],self.defaultAddress))
        
    def ReadOvertempEvents(self):
        
        return self.bytes_to_long(self.Send(self.param_read_overtemp_events,[],self.defaultAddress))
        
    def ReadRuntime(self):
        
        return self.bytes_to_long(self.Send(self.param_read_run_time,[],self.defaultAddress))
        
    def ReadExtendedFaults(self):
        
        return self.Send(self.param_read_extended_faults,[],self.defaultAddress)
        
    def ReadPIN(self):
        
        return self.Send(self.param_read_pin,[],self.defaultAddress)
        
    def ReadErrorCodes(self):
        
        return self.Send(self.param_read_error_codes,[],self.defaultAddress)
        
    def ReadColdplateTemp(self):
        
        return self.bytes_to_long(self.Send(self.param_read_cold_plate_temp,[],self.defaultAddress))
        
    def ReadSerialNumber(self):
        
        return self.bytes_to_long(self.Send(self.param_read_serial_number,[],self.defaultAddress))
    
    
        
