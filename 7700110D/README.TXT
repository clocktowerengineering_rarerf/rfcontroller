================================================================================
Navigator(TM) Virtual Front Panel (VFP) - ReadMe.Txt
================================================================================

This file contains important supplementary and late-breaking information 
that may not appear in the main product documentation. We recommend that 
you read this file in its entirety.

Sections of this file may contain information that applies only to 
specific software options. Such sections are clearly marked with the name 
of the Navigator VFP version (Basic, Full-Function, etc.) to which the 
information applies.



CONTENTS
================================================================================

-- INSTALLING VIRTUAL FRONT PANEL

-- RELEASE NOTES INCLUDED WITH THIS PRODUCT

-- KNOWN ISSUES IN THIS RELEASE

-- PRODUCT INFORMATION ON THE WEB

================================================================================



INSTALLING VIRTUAL FRONT PANEL
================================================================================

Requirements: Windows 2000/XP, One or more COM ports. Monitor resolution 
640 x 480 (preferred 800 x 600), Windows-compatible mouse or other 
pointing device, access to a CD-ROM drive, minimum of 10MB hard disk 
space, system memory of 32MB (64MB recommended), Intel Pentium(TM) class 
system 90 MHz or higher (Pentium 133 MHz recommended)

To Install Virtual Front Panel:

Note: We recommend closing all applications before installation.

1. Start Windows.

2. Insert Virtual Front Panel's CD-ROM into the CD-ROM drive. The setup 
program should start automatically. If it does not, locate the CD-ROM 
drive in Windows Explorer and double-click the setup.exe program.

3. Follow the on-screen instructions to complete installation. Virtual 
Front Panel installs by default to the location: 
C:\Program Files\Advanced Energry\Navigator VFP. You can select another 
location if you wish.

For more information, refer to the Navigator Virtual Front Panel 
electronic manual that installs with the software.


RELEASE NOTES INCLUDED WITH THIS PRODUCT
================================================================================
The electronic pdf manual requires Adobe Acrobat(r) Reader for viewing.


KNOWN ISSUES IN THIS RELEASE
================================================================================

Establishing Connection
--------------------------------------------------------------------------------
If the Navigator match is off when the host computer attempts to connect, 
Virtual Front Panel opens in Demonstration mode. After the Navigator 
power supply is powered up, VFP will be able to connect.

Windows XP
--------------------------------------------------------------------------------
Some Windows XP themes may cause minor cosmetic display issues with the
Virtual Front Panel. 

Viewing the HTML Manual
--------------------------------------------------------------------------------
To view the HTML version of the User Manual, you must have Netscape Navigator(r)
or Microsoft Internet Explorer(r) Version 4.0 or later.

Viewing The HTML Manual with Netscape Navigator 6.0
--------------------------------------------------------------------------------
When using Netscape Navigator 6.0 as your browser for viewing the HTML 
manual provided with this software, no scroll bar appears for use with 
any tab item selected in the left window pane. 

When using Netscape Navigator 6.0 as your browser for viewing the HTML 
manual, the search function that is found in the left hand window pane is 
not functional.

Both the scroll bar and search functionality works fine with previous 
versions of Netscape, as well as with all versions of Internet Explorer.


PRODUCT INFORMATION ON THE WEB
================================================================================
http://www.advanced-energy.com/

================================================================================
Copyright (c) 2000 Advanced Energy 
All rights reserved.

