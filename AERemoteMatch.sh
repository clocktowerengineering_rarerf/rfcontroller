#!/bin/bash -

# restarts the remote matchbox service, then restarts the local socat service after 5 seconds.

ssh pi@192.168.0.180 sudo systemctl restart ser2net.service

sleep 10

sudo /usr/bin/socat pty,echo=0,link=/dev/tty_REMOTEMATCH,group-late=dialout,mode=666, tcp:192.168.0.180:3000
sudo rm /home/rarerf/.wine/dosdevices/com41
ln -s /dev/tty_REMOTEMATCH /home/rarerf/.wine/dosdevices/com41
