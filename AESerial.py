import time
import serial
import binascii


class AESerial:


    nodeID = ""
    defaultBaud = 19200
    defaultTimeout = .2
    defaultPortID = '/dev/tty_MatchBox'
    defaultAddress = 1
    command_accepted = 0
    command_rejected_bad_parameter = 4
    
    
    # packet structure:
    # byte
    # 0               1               2               3               4           ....n 
    # 7 6 5 4 3 2 1 0 7 6 5 4 3 2 1 0 7 6 5 4 3 2 1 0 7 6 5 4 3 2 1 0 7 6 5 4 3 2 1 0 7 6 5 4 3 2 1 0 
    # |    HEADER    |    COMMAND    |    OPTIONAL   |   DATADATADATADATADATAetc   |     Checksum    |

    # HEADER INFO:
    # HEADER.Address: bits 7-3 are the address
    # HEADER.Length: bits 2-0 is the length of the data field

    # COMMAND INFO:
    # value in hex

    # OPTIONAL INFO:
    # in the case that HEADER.Length is 7, this defines the data length in bytes from 0 to 255.  Otherwise it doesn't exist.

    # DATA INFO:
    # data! bytes go 0 to n.

    # CHECKSUM INFO:
    # XOR of all bytes up to the checksum.
    
    
    def Checksum(self,bytestream):

        # grabs an incoming bytestring array and xors it.
        checksumByte = 0b0
        for n in range(0,len(bytestream)):
            checksumByte = checksumByte ^ bytestream[n]

        bytestream = bytearray([checksumByte])

        return bytestream

            


    def CreatePacket(self,command,data,address=defaultAddress):

        # creates a packet based on an incoming command, incoming data as a byte array, and address.

        dataLength = len(data)
        if(dataLength>6):
            headerLength = 7
            optionalByte = dataLength
            headerAddress = address << 3
            headerByte = headerAddress+headerLength
            bytestream = bytearray([headerByte,command,optionalByte])

        else:
            headerLength = dataLength
            headerAddress = address << 3
            headerByte = headerAddress+headerLength
            bytestream = bytearray([headerByte,command])

        

        bytestream = bytestream + bytearray(data)
        
        bytestream = bytestream + self.Checksum(bytestream)

        return bytestream

    def Send(self,command,data,address=defaultAddress):
        # need to follow the transaction in 4-73.
        sendstring = self.CreatePacket(command,data,address)
        self.AESerial.write(sendstring)
        ACKNAK = self.AESerial.read(1)
        
        if ACKNAK != '\x06' :
			print 'Bad Checksum or No Response.'
			# return whatever is on the checksum
			response = self.AESerial.readline()
			print response
			raise Exception
        else : 
			# first byte is the header byte.  Grab the header byte and let's figure out whether it's super long or just kind of long.
            headerbyte = self.AESerial.read(1)
            databytes = ord(headerbyte) - ((ord(headerbyte)>>3)<<3)
            commandbyte = self.AESerial.read(1)
			
            if databytes==7:
				
				#ok, it's longer than 6.
				# grab the next byte for datalength, and add 1 for checksum
                additionaldatabyte = self.AESerial.read(1)
                datalength = ord(additionaldatabyte)
            else:
				# ok, use databytes as datalength and add 1 for checksum
				
                datalength = databytes
            
			
			# ok, great, grab that many bytes from the buffer
			
            response = self.AESerial.read(datalength)
            
            # and now grab the checksum from the buffer
            checksumbyte = self.AESerial.read(1)
            
			
			# great, check that the checksum makes some modicum of sense
            if databytes==7:
                messagechecksum = self.Checksum(bytearray([headerbyte,commandbyte,additionaldatabyte])+bytearray(response))
            else:
                messagechecksum = self.Checksum(bytearray([headerbyte,commandbyte])+bytearray(response))
			
            if messagechecksum!=checksumbyte:
				
                print "the checksum doesn't match! calculated checksum"
                print ord(messagechecksum)
                print "sent checksum"
                print ord(checksumbyte)
                
            if self.AESerial.in_waiting > 0:
                print "uhhhh the buffer still has some number of bytes here."
                print self.AESerial.in_waiting
			
				
				
			# ok, neato, send ack
			
            self.AESerial.write('\x06')

	
        return response



    
    
    # data to byte array conversion, taken from https://stackoverflow.com/questions/8730927/convert-python-long-int-to-fixed-size-byte-array
    def long_to_bytes (self,val,length=0, endianness='little'):
        """
        Use :ref:`string formatting` and :func:`~binascii.unhexlify` to
        convert ``val``, a :func:`long`, to a byte :func:`str`.

        :param long val: The value to pack

        :param str endianness: The endianness of the result. ``'big'`` for
          big-endian, ``'little'`` for little-endian.


        Using :ref:`string formatting` lets us use Python's C innards.
        """

    # if length is specified, force width to 8*length
    
        if length!=0:
            width = length*8
            
        else:
        
        # calculate the width from the value provided    
        # one (1) hex digit per four (4) bits
            width = val.bit_length()
        
        
        # unhexlify wants an even multiple of eight (8) bits, but we don't
        # want more digits than we need (hence the ternary-ish 'or')
            width += 8 - ((width % 8) or 8)

    # format width specifier: four (4) bits per hex digit
        fmt = '%%0%dx' % (width // 4)

    # prepend zero (0) to the width, to zero-pad the output
        s = binascii.unhexlify(fmt % val)

        if endianness == 'little':
        # see http://stackoverflow.com/a/931095/309233
            s = s[::-1]

        return s
        
    def bytes_to_long(self, val, endianness='little'):
        
        if endianness=='big':
            
            val = val[::-1]
        
        
        output = 0
        
        for i in range(0,len(val)):
            
            output= output + ord(val[i])*(16**(2*i))
        
        return output
        

        
        
        

    
    # open the serial object.  Send everything in bytes.  
    def StartSerial(self,baud=defaultBaud,serialPort=defaultPortID,timeout=defaultTimeout, parity=serial.PARITY_ODD):
        self.AESerial = serial.Serial()
        self.AESerial.timeout=timeout
        self.AESerial.port=serialPort
        self.AESerial.baudrate=baud
        self.AESerial.parity=parity
        self.AESerial.open()      
        return self.AESerial
        
    def CloseSerial(self):
        self.AESerial.close()

    def BoundInput(self,value,lowerbound,upperbound):
        
        if value<lowerbound:
            
            print("value was too low, increased to: " + str(lowerbound))
            value = lowerbound
        
        elif value>upperbound:
            print("value was too high, decreased to: " + str(upperbound))
            value = upperbound
            
        return value
