import time
import serial
from AESerial import AESerial

class AEMatchbox(AESerial):



    defaultBaud = 19200
    defaultTimeout = .2
    defaultPortID = '/dev/tty_MatchBox'
    defaultAddress = 2
    
    # defines and such, parameter dictionary, etc

    # sets
    param_set_impedance_target = 71 # 4 bytes, 1 response.  Page 4-28
    param_set_autotune_setup = 82 #27 bytes, 1 response. Pages 4-29 to 4-31.
    param_set_capacitor_limits = 84 # 8 bytes, 1 respnse.  1000 = 100%. bytes 0-1 are min load, bytes 2-3 are min tune, bytes 4-5 is max load, 6-7 are max tune
    param_set_sensor_params = 88 #3 or 4 bytes, 1 response, check 4-33.
    param_set_presets_and_trajectories = 90 #check 4-34
    param_set_select_preset = 91 #selects a preset (1-4). 1 response
    param_set_operating_mode = 93 #0 is manual control, 1 is automatic, 2 is serial port control, #3 is preset control. 1 byte response
    param_set_enable_presets = 94 #0 disable, 1 enable, 1 byte response
    param_set_select_preset = 97 #0 internal, 1 external, 1 byte response
    param_set_disable_motor = 98 #0 enable, 1 disable, 1 byte response
    param_set_preset_and_trajectory_delays = 100 #check 4-38, 1 byte response
    param_move_load_to_position = 112 #2 bytes 0 to 10000, 1 byte response (CSR)
    param_set_baud = 117 # don't use sheesh
    param_move_tune_to_position = 122 #2 bytes 0 to 10000, 1 byte response (CSR)
    param_init_capacitors = 125 #homes caps, no data, 1 byte response
    param_set_bus_address = 127 # don't use unless you're smart, check 4-40
    
    # reads and gets
    
    param_report_match_type = 128 # reports match type, returns 20 bytes in ascii
    param_report_software_number = 130 # returns 7 ascii bytes of software PN
    param_report_status = 131 # check bits from report_bits below
    param_report_xilinx_firmware_version_rev = 132 # returns 11 ascii bytes of firmware rev
    param_report_motor_moving = 135 #0x01 tune motor moving, 0x02 load motor moving, 0x03 both moving
    param_report_target_impedance = 141 # divide values by 20.48 to get ohms.  bytes 0 and 1 are real impedance, 2 and 3 is imaginary
    param_report_autotune_setup = 152 #go see 4-43
    param_report_capacitor_limits = 154 # see 4-46
    param_report_sensor_setup = 158 # see 4-47
    param_report_preset_trajectories = 160 # see 4-48
    param_report_selected_preset = 161 # returns 1,2,3,4 depending on preset selected
    param_report_operating_mode = 163 # 0 analog, 1 auto, 2 serial, 3 preset
    param_report_preset_status = 164 # 1 enabled, 0 disabled
    param_report_capacitor_values = 166 # 8 bytes, see 4-50
    param_report_external_preset_select_status = 167 #0 internal preset, 1 external preset
    param_report_motor_disabled = 168 #0 enabled motors, 1 disabled motors
    param_report_personality_number = 169 #the hell??? 
    param_report_preset_trajectory_delays = 170 # see 4-51
    param_report_capacitor_positions = 180 # reported in hundredths of percent, bytes 0 and 1 are load cap, 2 and 3 tune cap
    param_report_specified_capacitor_positions = 184 #similar to above but also has some other absolute bullshit
    param_report_sensors_calculated_data = 185 # this is sort of useful, go look at 4-52
    param_report_software_version = 198 #3 ascii bytes
    param_report_unit_serial_number = 231 # 4 8 bit values
    param_report_fault_status = 254 # 8 bytes coming back, use the status bitmask below to generate values
    
    
    # status report bitmask, value is decimal
    status_bitmask_RF_ON = 1
    status_bitmask_tuned = 2
    status_bitmask_automatic_mode = 4
    status_bitmask_serial_mode = 8
    status_bitmask_manual_mode = 16
    status_bitmask_preset_enabled = 32
    status_bitmask_external_preset_selected = 64
    status_bitmask_generic_fault = 128
    status_bitmask_preset_active = 256
    status_bitmask_motor_disabled = 512
    status_bitmask_DC_fault = 16777216
    status_bitmask_EEPROM_fault = 33554432
    status_bitmask_NOVRAM_fault = 67108864
    status_bitmask_temp_fault = 134217728
    status_motor_init_fault = 2147483648
    
    #fault bitmask, value is decimal
    fault_bitmask_low_DC_voltage = 1
    fault_bitmask_overvoltage = 2
    fault_bitmask_fan_too_slow = 4
    fault_bitmask_overtemp = 8
    fault_bitbask_overcurrent = 16
    fault_bitmask_fan2_too_slow = 128
    fault_bitmask_fan3_too_slow = 256
    fault_bitmask_motor_init_fault = 8192
    fault_bitmask_drip_sensor = 16384
    
    
    #other defines
    boundary_percentage_hundredths_upper = 10000
    mode_manual_control = 0
    mode_automatic_control = 1
    mode_serial_control = 2
    mode_preset_control = 3
    rf_input_sensor = 0
    rf_output_sensor = 0
    
    

    def GetSensorData(self,sensor=rf_input_sensor):
        
        
        if self.rf_output_sensor:
            
            response = self.Send(self.param_report_sensors_calculated_data,[1],self.defaultAddress)
    
        else:
            
            response = self.Send(self.param_report_sensors_calculated_data,[0],self.defaultAddress)

        sensorData = { 'ohm':50.0, 'x' : 0.0, 'gamma' : 0.0, 'watts' : 0.0}
        
        sensorData['ohm'] = float(self.bytes_to_long(response[1:3]))
        if sensorData['ohm'] > 32767:
            print "invert ohm"
            sensorData['ohm'] = (65535 - sensorData['ohm']) * -1
        sensorData['ohm']= float(sensorData['ohm']*50/1024)

        sensorData['x'] = self.bytes_to_long(response[3:5])
        if sensorData['x'] > 32767:
            print "invert"
            sensorData['x'] = (65535 - sensorData['x']) * -1
        sensorData['x']= float(sensorData['x']*50/1024)

        sensorData['gamma']= (float(self.bytes_to_long(response[5:7]))/1024)**.5
        
        sensorData['watts'] = self.bytes_to_long(response[7:9])
        
        print sensorData
        
        return sensorData
        
        
    def SetControlMode(self,mode):
        
        mode = self.BoundInput(mode,0,self.mode_preset_control)
        self.Send(self.param_set_operating_mode,self.long_to_bytes(mode,1),self.defaultAddress)

        
    def SetLoadCapPosition(self,load):

        load = self.BoundInput(load,0,self.boundary_percentage_hundredths_upper)
        #print load
        self.Send(self.param_move_load_to_position,self.long_to_bytes(load,2),self.defaultAddress)
        
        
    def SetTuneCapPosition(self,tune):

        tune = self.BoundInput(tune,0,self.boundary_percentage_hundredths_upper)
        #print tune
        self.Send(self.param_move_tune_to_position,self.long_to_bytes(tune,2),self.defaultAddress)
        
        
    def GetCapPosition(self):
        
        response = self.Send(self.param_report_capacitor_positions,[0],self.defaultAddress)
        capacitorPosition = dict()
        capacitorPosition['load'] = self.bytes_to_long(response[0:2])*.01
        capacitorPosition['tune'] = self.bytes_to_long(response[2:4])*.01
        #print capacitorPosition
        return capacitorPosition
        
    def GetFaults(self):
        value = self.Send(self.param_report_fault_status,[],self.defaultAddress)
        #print(self.bytes_to_long(value))
        return value

    def GetStatus(self):
        value = self.Send(self.param_report_status,[],self.defaultAddress)
        #print(self.bytes_to_long(value))
        return value


