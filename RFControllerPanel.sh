#!/bin/bash
#
TANGO_HOME=/usr;		export TANGO_HOME
sleep 15s
if [ ! $TANGO_HOST ] && [ -f /etc/tangorc ]; then
   . /etc/tangorc
fi

#---------------------------------------------------------
#       Set the Class Path for Tango and AtkPanel usage
#---------------------------------------------------------
LIB_DIR=/usr/share/java;   export LIB_DIR


TANGO=$LIB_DIR/JTango.jar
TANGOATK=$LIB_DIR/ATKCore.jar:$LIB_DIR/ATKWidget.jar
ATKPANEL=$LIB_DIR/atkpanel.jar

CLASSPATH=$ATKPANEL:$TANGOATK:$TANGO
export CLASSPATH

#---------------------------------------------------------
#	Start the atkpanel process
#---------------------------------------------------------

/usr/bin/java -mx128m -DTANGO_HOST=$TANGO_HOST atkpanel.MainPanel $*
